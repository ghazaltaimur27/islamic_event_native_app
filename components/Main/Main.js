
import React, {Component} from 'react';
import { StyleSheet, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import bgSrc from '../../assets/images/background.png';

export default class Main extends Component {
  
  buttonClicked = () => {
    this.props.navigation.navigate('Login')
  }

  render() {
    return (
      <TouchableWithoutFeedback onPressOut={this.buttonClicked}>
        <ImageBackground style={styles.picture} source={bgSrc}></ImageBackground>
      </TouchableWithoutFeedback>
      
    );
  }
}

const styles = StyleSheet.create({
  picture: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover'
  },
});