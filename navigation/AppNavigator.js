import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Login from '../components/Login/Login';
import Main from '../components/Main/Main';

const RootStack = createStackNavigator(
  {
    Main: {
      screen: Main, 
      navigationOptions: {
          header: null,
      },
    },
    Login: Login,
  },
  {
    initialRouteName: 'Main',
  }
);

const AppContainer = createAppContainer(RootStack);
export default class AppNavigator extends React.Component {
  render() {
    return <AppContainer />;
  }
}